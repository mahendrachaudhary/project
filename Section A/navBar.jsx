import React, { Component } from "react";
class NavBar extends Component {
  render() {
    const { getNewBill } = this.props;
    return (
      <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
        <a className="navbar-brand" href="#">
          BillingSystem
        </a>
        <div className="" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <a className="nav-link" href="#" onClick={() => getNewBill()}>
                New Bill
              </a>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
export default NavBar;
