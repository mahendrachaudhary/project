import React, { Component } from "react";
class NewBill extends Component {
  state = {
    products: this.props.products,
    product: { category: "", instock: "", price: "" },
    categories: ["Beverages", "Chocolates", "Biscuits"],
    instocks: ["Yes", "No"],
    priceRange: ["<10", "10-20", ">20"],
    filterProducts: { ...this.products },
  };
  handleChange = (e) => {
    let s1 = { ...this.state };
    let { currentTarget: input } = e;
    s1.product[input.name] = input.value;
    this.setState(s1);
  };
  showTable = (onSorting, setSort, updateBill) => {
    let s1 = { ...this.state };
    let pro1 = s1.product.category
      ? s1.products.filter((p) =>
          s1.product.category !== "" ? p.category === s1.product.category : p
        )
      : s1.products;
    let pro2 = s1.product.instock
      ? pro1.filter((p) => p.instock === s1.product.instock)
      : pro1;
    let pro3 = s1.product.price
      ? pro2.filter((p) =>
          s1.product.price === "<10"
            ? p.price < 10
            : s1.product.price === "10-20"
            ? p.price > 9 && p.price < 21
            : p.price > 20
        )
      : pro2;
    s1.filterProducts = pro3;
    return (
      <div className="text-center">
        <div className="row bg-dark text-white">
          <div className="col-2" onClick={() => onSorting(0)}>
            {setSort === 0 ? "Code(X)" : "Code"}
          </div>
          <div className="col-2" onClick={() => onSorting(1)}>
            {setSort === 1 ? "Product(X)" : "Product"}
          </div>
          <div className="col-2" onClick={() => onSorting(2)}>
            {setSort === 2 ? "Category(X)" : "Category"}
          </div>
          <div className="col-2" onClick={() => onSorting(3)}>
            {setSort === 3 ? "Price(X)" : "Price"}
          </div>
          <div className="col-2" onClick={() => onSorting(4)}>
            {setSort === 4 ? "In Stock(X)" : "In Stock"}
          </div>
          <div className="col-2"></div>
        </div>
        {pro3.map((p, index) => {
          const { code, prod, price, instock, category } = p;
          return (
            <div className="row border">
              <div className="col-2">{code}</div>
              <div className="col-2">{prod}</div>
              <div className="col-2">{category}</div>
              <div className="col-2">{price}</div>
              <div className="col-2">{instock}</div>
              <div className="col-2">
                <button
                  className="btn btn-dark"
                  onClick={() => updateBill(index)}
                >
                  Add to Bill
                </button>
              </div>
            </div>
          );
        })}
      </div>
    );
  };
  render() {
    const { categories, instocks, priceRange, product } = this.state;
    const {
      onSorting,
      setSort,
      updateBill,
      currentBill,
      updateQuantity,
      closeBill,
    } = this.props;
    return (
      <div className="container">
        <h4>Details of Current Bill</h4>
        <span>
          Items:{currentBill.length},Quantity:
          {currentBill.length > 0
            ? currentBill.reduce((acc, curr) => acc + curr.quantity, 0)
            : 0}
          ,Amount:
          {currentBill.length > 0
            ? currentBill.reduce((acc, curr) => acc + curr.value, 0)
            : 0}
        </span>
        {currentBill.length > 0 ? (
          <div>
            {currentBill.map((p, index) => (
              <div className="row border">
                <div className="col-6">
                  {p.code},{p.prod},Price:{p.price},Quantity:{p.quantity},Value:
                  {p.value}
                </div>
                <div>
                  <button
                    className="btn btn-success btn-sm mr-1"
                    onClick={() => updateQuantity(index, "+")}
                  >
                    +
                  </button>
                  <button
                    className="btn btn-warning btn-sm mr-1"
                    onClick={() => updateQuantity(index, "-")}
                  >
                    -
                  </button>
                  <button
                    className="btn btn-danger btn-sm"
                    onClick={() => updateQuantity(index, "X")}
                  >
                    X
                  </button>
                </div>
              </div>
            ))}
            <button
              className="btn btn-primary btn-sm"
              onClick={() => closeBill()}
            >
              Close Bill
            </button>
          </div>
        ) : (
          ""
        )}
        <div className="text-center">
          <h4>Product List</h4>
        </div>
        <div className="row">
          <label className="font-weight-bold mr-5">Filter Products by : </label>
          {this.showDropdown(
            "Category",
            categories,
            "category",
            product.category
          )}
          {this.showDropdown("In Stock", instocks, "instock", product.instock)}
          {this.showDropdown("Price Range", priceRange, "price", product.price)}
        </div>
        {this.showTable(onSorting, setSort, updateBill)}
      </div>
    );
  }
  showDropdown = (label, arr, name, selValue) => {
    return (
      <div className="form-group w-25 mr-2">
        <select
          className="form-control"
          name={name}
          value={selValue}
          onChange={this.handleChange}
        >
          <option value="">Select The {label}</option>
          {arr.map((opt) => (
            <option>{opt}</option>
          ))}
        </select>
      </div>
    );
  };
}
export default NewBill;
