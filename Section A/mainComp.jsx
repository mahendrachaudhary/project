import React, { Component } from "react";
import NavBar from "./navBar";
import NewBill from "./newBill";
class MainComp extends Component {
  state = {
    products: [
      {
        code: "PEP221",
        prod: "Pepsi",
        price: 12,
        instock: "Yes",
        category: "Beverages",
      },
      {
        code: "COK113",
        prod: "Coca Cola",
        price: 18,
        instock: "Yes",
        category: "Beverages",
      },
      {
        code: "MIR646",
        prod: "Mirinda",
        price: 15,
        instock: "No",
        category: "Beverages",
      },
      {
        code: "SLI874",
        prod: "Slice",
        price: 22,
        instock: "Yes",
        category: "Beverages",
      },
      {
        code: "MIN654",
        prod: "Minute Maid",
        price: 25,
        instock: "Yes",
        category: "Beverages",
      },
      {
        code: "APP652",
        prod: "Appy",
        price: 10,
        instock: "No",
        category: "Beverages",
      },
      {
        code: "FRO085",
        prod: "Frooti",
        price: 30,
        instock: "Yes",
        category: "Beverages",
      },
      {
        code: "REA546",
        prod: "Real",
        price: 24,
        instock: "No",
        category: "Beverages",
      },
      {
        code: "DM5461",
        prod: "Dairy Milk",
        price: 40,
        instock: "Yes",
        category: "Chocolates",
      },
      {
        code: "KK6546",
        prod: "Kitkat",
        price: 15,
        instock: "Yes",
        category: "Chocolates",
      },
      {
        code: "PER5436",
        prod: "Perk",
        price: 8,
        instock: "No",
        category: "Chocolates",
      },
      {
        code: "FST241",
        prod: "5 Star",
        price: 25,
        instock: "Yes",
        category: "Chocolates",
      },
      {
        code: "NUT553",
        prod: "Nutties",
        price: 18,
        instock: "Yes",
        category: "Chocolates",
      },
      {
        code: "GEM006",
        prod: "Gems",
        price: 8,
        instock: "No",
        category: "Chocolates",
      },
      {
        code: "GD2991",
        prod: "Good Day",
        price: 25,
        instock: "Yes",
        category: "Biscuits",
      },
      {
        code: "PAG542",
        prod: "Parle G",
        price: 5,
        instock: "Yes",
        category: "Biscuits",
      },
      {
        code: "MON119",
        prod: "Monaco",
        price: 7,
        instock: "No",
        category: "Biscuits",
      },
      {
        code: "BOU291",
        prod: "Bourbon",
        price: 22,
        instock: "Yes",
        category: "Biscuits",
      },
      {
        code: "MAR951",
        prod: "MarieGold",
        price: 15,
        instock: "Yes",
        category: "Biscuits",
      },
      {
        code: "ORE188",
        prod: "Oreo",
        price: 30,
        instock: "No",
        category: "Biscuits",
      },
    ],

    currentBill: [],
    newBill: false,
    setSort: -1,
  };
  hanldeCloseBill = () => {
    let s1 = { ...this.state };
    s1.currentBill = [];
    s1.newBill = false;
    this.setState(s1);
  };
  handleBill = (index) => {
    let s1 = { ...this.state };
    let product = { code: "", prod: "", price: "", quantity: "", value: "" };
    product.code = s1.products[index].code;
    product.prod = s1.products[index].prod;
    product.price = s1.products[index].price;
    product.quantity = 1;
    product.value = s1.products[index].price;

    if (s1.currentBill.length > 0) {
      let pIndex = s1.currentBill.findIndex(
        (p) => s1.products[index].code === p.code
      );
      if (pIndex >= 0) {
        s1.currentBill[pIndex].quantity++;
        s1.currentBill[pIndex].value =
          s1.currentBill[pIndex].quantity * s1.currentBill[pIndex].price;
      } else {
        s1.currentBill.push(product);
      }
    } else {
      s1.currentBill.push(product);
    }
    this.setState(s1);
  };
  handleNewBill = () => {
    let s1 = { ...this.state };
    s1.newBill = true;
    this.setState(s1);
  };
  handleSorting = (colNo) => {
    let s1 = { ...this.state };
    switch (colNo) {
      case 0:
        s1.products.sort((t1, t2) => t1.code.localeCompare(t2.code));
        break;
      case 1:
        s1.products.sort((t1, t2) => t1.prod.localeCompare(t2.prod));
        break;
      case 2:
        s1.products.sort((t1, t2) => t1.category.localeCompare(t2.category));
        break;
      case 3:
        s1.products.sort((t1, t2) => +t1.price - +t2.price);
        break;
      case 4:
        s1.products.sort((t1, t2) => t1.instock.localeCompare(t2.instock));
        break;
      default:
        break;
    }
    s1.setSort = colNo;
    this.setState(s1);
  };
  handleQuantity = (index, value) => {
    let s1 = { ...this.state };
    if (value === "+") {
      s1.currentBill[index].quantity++;
      s1.currentBill[index].value =
        s1.currentBill[index].quantity * s1.currentBill[index].price;
    } else if (value === "-") {
      if (s1.currentBill[index].quantity === 1) {
        s1.currentBill.splice(index, 1);
      } else {
        s1.currentBill[index].quantity--;
        s1.currentBill[index].value =
          s1.currentBill[index].quantity * s1.currentBill[index].price;
      }
    } else if (value === "X") {
      s1.currentBill.splice(index, 1);
    }
    this.setState(s1);
  };
  render() {
    const { products, newBill, setSort, currentBill } = this.state;
    return (
      <div className="container">
        <NavBar getNewBill={this.handleNewBill} />
        <br />
        {newBill ? (
          <NewBill
            products={products}
            onSorting={this.handleSorting}
            setSort={setSort}
            updateBill={this.handleBill}
            currentBill={currentBill}
            updateQuantity={this.handleQuantity}
            closeBill={this.hanldeCloseBill}
          />
        ) : (
          ""
        )}
      </div>
    );
  }
}
export default MainComp;
