import React, { Component } from "react";
class NavBar extends Component {
  render() {
    let { view, changeView } = this.props;
    return (
      <nav className="navbar navbar-expand-sm navbar-light bg-light">
        <a className="navbar-brand" href="#">
          MyFavPizza
        </a>
        <div className="" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <a className="nav-link" href="#" onClick={() => changeView(0)}>
                Veg Pizza
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#" onClick={() => changeView(1)}>
                Non-Veg Pizza
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#" onClick={() => changeView(2)}>
                Side Dishes
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#" onClick={() => changeView(3)}>
                Other items
              </a>
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
export default NavBar;
