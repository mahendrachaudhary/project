import React, {Component} from "react";
class SideDishes extends Component {
    render () {
        const {data} = this.props;
        return <div className="row">
                {data.map((p)=>{
                    if(p.type==="SideDish"){
                        return <div className="col-6 border text-center">
                            <div>{<img src={p.image} style={{width: "230px"}}/>}</div>
                            <h6>{p.name}</h6>
                            <div>{p.desc}</div>
                        </div>
                    }
                })}
        </div>
    }
}
export default SideDishes;