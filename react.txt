1.	What is JSX?
	JSX stands for JavaScript XML.
	JSX is basically a syntax extension of regular JavaScript and is used to create React elements.
	These elements are then rendered to the React DOM.
	
	It is faster than normal JavaScript as it performs 
	optimizations while translating to regular JavaScript.
	It makes it easier for us to create templates.
	Instead of separating the markup and logic in separated files, React uses components for this purpose.


2.	What is React
	React is a JavaScript library created by Facebook. React is a User Interface (UI) library. 
	React is a tool for building UI components.
	

3.	What are features of React.
	React has virtual DOM, which is a lightweight copy of the actual DOM.
	For every object that exists in the original DOM, there is an object for that in the virtual DOM.
	Each Change updates the virtual DOM.
	Manipulating DOM is slow, but manipulating virtual DOM is fast as nothing is drawn on the screen.


4.	List some of the advantages of React
	React has a very small API to learn.
	Working with DOM API is difficult and cumbersome unlike Javascript react developer works with a virtual 
	DOM that is easier and friendlier.


5.	What is ReactDOM and what is the difference between ReactDOM and React?
	ReactDOM is s a package that provides DOM specific methods, the difference between ReactDOM and react is that 
	ReactDOM renders React Elements to the DOM, so it's for web development. 
	ReactNative renders React Elements to a native mobile platform. You can write your code to target both at the same time.	


6.	Differentiate between Real DOM and Virtual DOM.
	A virtual DOM object has the same properties as a real DOM object, 
	but it lacks the real thing's power to directly change what's on the screen. Manipulating the DOM is slow. 
	Manipulating the virtual DOM is much faster, because nothing gets drawn onscreen.


7.	How React works?
	React uses a declarative paradigm that makes it easier to reason about your 
	application and aims to be both efficient and flexible. 
	It designs simple views for each state in your application, 
	and React will efficiently update and render just the right component when your data changes


8.	Does ReactJS use HTML?
	With React, we write HTML using JavaScript. We rely on the power of JavaScript 
	to generate HTML that depends on some data, rather than enhancing HTML to make it work with that data. 
	Enhancing HTML is what other JavaScript frameworks usually do.


9.	What is the difference between state and props?
	The key difference between props and state is that state is internal and controlled by 
	the component itself while props are external and controlled by whatever renders the component.
	


10.	What is render() in React? And explain its purpose?
	In react, render is a method that tell react what to display. 
	return in a method or function is the output of the method or function.
	


11.	What is an event in React?
	An event is an action that could be triggered as a result of the user action or system generated event. 
	React events are named as camelCase instead of lowercase. 
	With JSX, a function is passed as the event handler instead of a string


12.	What are controlled and uncontrolled components in React?
	In a controlled component, form data is handled by a React component. 
	The alternative is uncontrolled components, where form data is handled by the DOM itself. 
	To write an uncontrolled component,instead of writing an event handler for every state update, 
	you can use a ref to get form values from the DOM.


13.	How are forms created in React?
	In React, mutable state is typically kept in the state property of components, and only updated with setState() . 
	We can combine the two by making the React state be the �single source of truth�. 
	Then the React component that renders a form also controls what happens in that form on subsequent user input.


14.	What is arrow function in React? How is it used?
	Arrow functions are shorthand functions which automatically bind to the surrounding context. 
	So we don't need to explicitly write. 



15.	What is virtual DOM in react. Explain it and its advantages.
	In React, for every DOM object, there is a corresponding �virtual DOM object.� 
	A virtual DOM object is a representation of a DOM object, like a lightweight copy. 
	A virtual DOM object has the same properties as a real DOM object, but it lacks the real thing's 
	power to directly change what's on the screen.
	Virtual DOM provides a more efficient way of updating the view in a web application. 
	Each time the underlying data changes in a React app, a new Virtual DOM representation of the user interface is created. 
	Rendering the Virtual DOM is always faster than rendering the UI in the actual browser DOM.


16.	What are the stateless components?
	STATELESS COMPONENT declared as a function that has no state and returns the same markup given the same props. 
	A quote from the React documentation: These components must not retain internal state, 
	do not have backing instances, and do not have the component lifecycle methods.



17.	What are react components
	Components are independent and reusable bits of code. They serve the same purpose as JavaScript functions, 
	but work in isolation and return HTML via a render() function. 
	Components come in two types, Class components and Function components.


18.	How do you create a react app?
	Following commands are used to create react app.
	npx create-react-app my-app
	cd my-app
	npm start


19.	What is React.Fragment?
	A common pattern in React is for a component to return multiple elements. 
	Fragments let you group a list of children without adding extra nodes to the DOM.


20.	How to update state in react
	To update state in react we use setState function.



21.	What is event and event handling.
	An event is an action that could be triggered as a result of the user action or system generated event. 
	React events are named as camelCase instead of lowercase.
	Handling events with React elements is very similar to handling events on DOM elements. 
	There are some syntax differences: React events are named using camelCase, rather than lowercase. 
	With JSX you pass a function as the event handler, rather than a string.


 
22.	How do we create an event in React?
	React events are written in camelCase syntax: onClick instead of onclick . React 
	event handlers are written inside curly braces: onClick={shoot} instead of onClick="shoot()" 


23.	What are props and how do use pass it?
	Props are arguments passed into React components. Props are passed to components via HTML attributes.


24.	What are React Developer Tools?
	React Developer Tools is a Chrome DevTools extension for the open-source React JavaScript library. 
	It allows you to inspect the React component hierarchies in the Chrome Developer Tools. 
	You will get two new tabs in your Chrome DevTools: "Components" and "Profiler".



25.	What are Forms in React and how we create a form in react?
	In React, mutable state is typically kept in the state property of components, and only updated with setState() . 
	We can combine the two by making the React state be the �single source of truth�. 
	Then the React component that renders a form also controls what happens in that form on subsequent user input.


26.	How we extract props in a component?
	We use object destructing method and extract the props.






